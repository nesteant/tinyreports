'use strict';
var lrSnippet = require('grunt-contrib-livereload/lib/utils').livereloadSnippet;
var mountFolder = function (connect, dir) {
  return connect.static(require('path').resolve(dir));
};

module.exports = function (grunt) {
  // load all grunt tasks
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);


  var config = {
    app: 'app',
    root: {
      scripts: 'app/scripts',
      styles: 'app/styles'
    },
    dist: 'dist',
    test: 'test'
  };

  try {
    config.app = require('./component.json').appPath || config.app;
  } catch (e) {
  }

  grunt.initConfig({
    config: config,
    watch: {
      less: {
        files: ['<%= config.app %>/{,*/}*.less'],
        tasks: ['less:dist']
      },
      livereload: {
        files: [
          '<%= config.app %>/{,*/}*.html',
          '{.tmp,<%= config.app %>}/styles/{,*/}*.css',
          '{.tmp,<%= config.app %>}/scripts/{,*/}*.js',
          '<%= config.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
        ],
        tasks: ['clean:dev', 'concat:dev','livereload']
      }
    },
    connect: {
      options: {
        port: 9000,
        // Change this to '0.0.0.0' to access the server from outside.
        hostname: 'localhost'
      },
      livereload: {
        options: {
          middleware: function (connect) {
            return [
              lrSnippet,
              mountFolder(connect, '.tmp'),
              mountFolder(connect, config.app)
            ];
          }
        }
      },
      test: {
        options: {
          middleware: function (connect) {
            return [
              mountFolder(connect, '.tmp'),
              mountFolder(connect, 'test')
            ];
          }
        }
      }
    },
    open: {
      server: {
        url: 'http://localhost:<%= connect.options.port %>'
      }
    },
    clean: {
      dev: {
        files: [
          {
            dot: true,
            src: ['.tmp', '<%= config.app %>/scripts/main.js']
          }
        ]
      },
      dist: {
        files: [
          {
            dot: true,
            src: [
              '.tmp',
              '<%= config.dist %>/*',
              '!<%= config.dist %>/.git*'
            ]
          }
        ]
      },
      server: '.tmp'
    },
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      all: [
        'Gruntfile.js',
        '<%= config.app %>/scripts/{,*/}*.js'
      ]
    },
    karma: {
      unit: {
        configFile: 'karma.conf.js',
        singleRun: true
      }
    },
    less: {
      dist: {
        options: {
          paths: ['<%= config.app %>/styles'],
          yuicompress: true,
          strictImports: true
        },
        files: {
          '<%= config.app %>/styles/main.css': '<%= config.app %>/styles/main.less'
        }
      }
    },
    concat: {
      dev: {
        files: {
          '<%= config.app %>/scripts/main.js': [
            '.tmp/scripts/{,*/}*.js',
            '<%= config.app %>/scripts/{,*/}*.js'
          ]
        }
      },
      dist: {
        files: {
          '<%= config.dist %>/scripts/main.js': [
            '.tmp/scripts/{,*/}*.js',
            '<%= config.app %>/scripts/{,*/}*.js'
          ]
        }
      }
    },
    useminPrepare: {
      html: '<%= config.app %>/index.html',
      options: {
        dest: '<%= config.dist %>'
      }
    },
    usemin: {
      html: ['<%= config.dist %>/{,*/}*.html'],
      css: ['<%= config.dist %>/styles/{,*/}*.css'],
      options: {
        dirs: ['<%= config.dist %>']
      }
    },
    imagemin: {
      dist: {
        files: [
          {
            expand: true,
            cwd: '<%= config.app %>/images',
            src: '{,*/}*.{png,jpg,jpeg}',
            dest: '<%= config.dist %>/images'
          }
        ]
      }
    },
    cssmin: {
      dist: {
        files: {
          '<%= config.dist %>/styles/main.css': [
            '.tmp/styles/{,*/}*.css',
            '<%= config.app %>/styles/{,*/}*.css'
          ]
        }
      }
    },
    htmlmin: {
      dist: {
        options: {
          /*removeCommentsFromCDATA: true,
           // https://github.com/yeoman/grunt-usemin/issues/44
           //collapseWhitespace: true,
           collapseBooleanAttributes: true,
           removeAttributeQuotes: true,
           removeRedundantAttributes: true,
           useShortDoctype: true,
           removeEmptyAttributes: true,
           removeOptionalTags: true*/
        },
        files: [
          {
            expand: true,
            cwd: '<%= config.app %>',
            src: ['*.html', 'views/*.html'],
            dest: '<%= config.dist %>'
          }
        ]
      }
    },
    cdnify: {
      dist: {
        html: ['<%= config.dist %>/*.html']
      }
    },
    ngmin: {
      dist: {
        files: [
          {
            expand: true,
            cwd: '<%= config.dist %>/scripts',
            src: '*.js',
            dest: '<%= config.dist %>/scripts'
          }
        ]
      }
    },
    uglify: {
      dist: {
        files: {
          '<%= config.dist %>/scripts/main.js': [
            '<%= config.dist %>/scripts/main.js'
          ]
        }
      }
    },
    rev: {
      dist: {
        files: {
          src: [
            '<%= config.dist %>/scripts/{,*/}*.js',
            '<%= config.dist %>/styles/{,*/}*.css',
            '<%= config.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
            '<%= config.dist %>/styles/fonts/*'
          ]
        }
      }
    },
    copy: {
      dist: {
        files: [
          {
            expand: true,
            dot: true,
            cwd: '<%= config.app %>',
            dest: '<%= config.dist %>',
            src: [
              '*.{ico,txt}',
              '.htaccess',
              'components/**/*',
              'images/{,*/}*.{gif,webp}',
              'styles/fonts/*'
            ]
          }
        ]
      }
    }
  });

  grunt.renameTask('regarde', 'watch');

  grunt.registerTask('server', [
    'clean:server',
    'less:dist',
    'livereload-start',
    'connect:livereload',
    'open',
    'watch'
  ]);

  grunt.registerTask('test', [
    'clean:server',
    'connect:test',
    'karma'
  ]);
  grunt.registerTask('dev', [
    'clean:dev',
    'jshint',
    'less:dist',
    'useminPrepare',
    'imagemin',
    'cssmin',
    'htmlmin',
    'concat:dev',
    'copy',
    'livereload-start',
    'connect:livereload',
    'open',
    'watch'
  ]);
  grunt.registerTask('build', [
    'clean:dist',
    'jshint',
    'test',
    'less:dist',
    'useminPrepare',
    'imagemin',
    'cssmin',
    'htmlmin',
    'concat:dist',
    'copy',
    'cdnify',
    'ngmin',
    'uglify',
    'rev',
    'usemin'
  ]);

  grunt.registerTask('default', ['build']);
};
