'use strict';

var appConfig = {
  domain: "https://thelightapps.com"
};

angular.module('websiteApp', [])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
