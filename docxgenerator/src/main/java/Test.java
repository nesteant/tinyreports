import org.docx4j.XmlUtils;
import org.docx4j.convert.in.xhtml.XHTMLImporter;
import org.docx4j.convert.out.html.AbstractHtmlExporter;
import org.docx4j.convert.out.html.AbstractHtmlExporter.HtmlSettings;
import org.docx4j.convert.out.html.HtmlExporterNG2;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.NumberingDefinitionsPart;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * @author Anton Nesterenko
 * @since 0.5.3
 */
public class Test {


	public static void main(String[] args) throws Exception {
		String inputfilepath = "D:/render.html";

		XHTMLImporter.setHyperlinkStyle("Hyperlink");

		WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.createPackage();

		NumberingDefinitionsPart ndp = new NumberingDefinitionsPart();
		wordMLPackage.getMainDocumentPart().addTargetPart(ndp);
		ndp.unmarshalDefaultNumbering();

		if (inputfilepath.endsWith("html")) {
			wordMLPackage.getMainDocumentPart().getContent().addAll(
					XHTMLImporter.convert(new File(inputfilepath), null, wordMLPackage));

		} else if (inputfilepath.endsWith("docx")) {
			//Round trip docx -> XHTML -> docx
			WordprocessingMLPackage docx = WordprocessingMLPackage.load(new File(inputfilepath));
			AbstractHtmlExporter exporter = new HtmlExporterNG2();

			// Use file system, so there is somewhere to save images (if any)
			OutputStream os = new FileOutputStream(inputfilepath + ".html");

			HtmlSettings htmlSettings = new HtmlSettings();
			htmlSettings.setImageDirPath(inputfilepath + "_files");
//			htmlSettings.setImageTargetUri((inputfilepath.lastIndexOf("d:/_files/");

			javax.xml.transform.stream.StreamResult result = new javax.xml.transform.stream.StreamResult(os);
			exporter.html(docx, result, htmlSettings);

			wordMLPackage.getMainDocumentPart().getContent().addAll(
					XHTMLImporter.convert(new File(inputfilepath + ".html"), null, wordMLPackage));
		} else {
			return;
		}

		System.out.println(
				XmlUtils.marshaltoString(wordMLPackage.getMainDocumentPart().getJaxbElement(), true, true));

		wordMLPackage.save(new java.io.File("d:/html_output.docx"));
	}
}
